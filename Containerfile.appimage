# Build an environment for creating an AppImage.

# Use a distro with an old libc to maximize support on as many linux distros
# and architectures as possible.
FROM ubuntu:14.04

ARG ARCH

RUN apt-get -qq -y update && \
    apt-get -qq -y dist-upgrade && \
    apt-get -qq -y install bison \
                   bzip2 \
                   curl \
                   file \
                   flex \
                   gcc \
                   libc6-dev \
                   libbsd-dev \
                   libncurses5-dev \
                   make \
                   wget \
                   xz-utils && \
    apt-get -qq -y clean && \
    apt-get -qq -y autoclean

WORKDIR /usr/local
ARG APPIMAGEPATH="AppImage/AppImageKit/releases/download/13"
ARG APPIMAGEFILENAME="appimagetool-${ARCH}.AppImage"
ARG APPIMAGEURL="https://github.com/${APPIMAGEPATH}/${APPIMAGEFILENAME}"
RUN curl -OL ${APPIMAGEURL} && \
    chmod +x appimagetool-${ARCH}.AppImage && \
    ./appimagetool-${ARCH}.AppImage --appimage-extract && \
    chmod 0755 squashfs-root && \
    chmod 0755 squashfs-root/usr && \
    chmod 0755 squashfs-root/usr/bin && \
    chmod 0755 squashfs-root/usr/lib && \
    chmod 0755 squashfs-root/usr/lib/appimagekit && \
    chmod 0755 squashfs-root/usr/share

ARG OPENSSLVER="1.1.1s"
RUN wget -O /tmp/openssl-${OPENSSLVER}.tar.gz -q --no-check-certificate \
         https://www.openssl.org/source/openssl-${OPENSSLVER}.tar.gz && \
    tar -C /usr/local/src -xf /tmp/openssl-${OPENSSLVER}.tar.gz
WORKDIR /usr/local/src/openssl-${OPENSSLVER}
RUN ./config -fPIC shared && \
    make -s -j$(nproc) && \
    make -s install

# PostgreSQL 10 is the first release supporting multiple hosts in the
# connection string.
ARG PGVER="10.23"
ARG PGFILENAME="postgresql-${PGVER}.tar.bz2"
ARG PGURL="https://ftp.postgresql.org/pub/source/v${PGVER}/${PGFILENAME}"
RUN wget -O /tmp/postgresql-${PGVER}.tar.bz2 -q --no-check-certificate \
         ${PGURL}
RUN tar -C /usr/local/src -xf /tmp/postgresql-${PGVER}.tar.bz2
WORKDIR /usr/local/src/postgresql-${PGVER}
RUN ./configure --silent --without-ldap --without-readline --without-zlib \
    --without-gssapi --with-openssl --prefix=/usr && \
    make -s -j $(nproc) install && \
    ldconfig

ARG CMAKEVER="3.1.3"
RUN curl -o /tmp/cmake-${CMAKEVER}-linux-${ARCH}.tar.gz -SsOL \
         https://github.com/Kitware/CMake/releases/download/v${CMAKEVER}/cmake-${CMAKEVER}-linux-${ARCH}.tar.gz && \
    tar -C /usr --strip-components=1 \
        -xf /tmp/cmake-${CMAKEVER}-linux-${ARCH}.tar.gz
