This is a collection of scripts to use Podman for specific tasks:

* build-appimage - Create an AppImage for pg_top.
* build-appimage-container - Build a container image to use for creating an
          AppImage.
